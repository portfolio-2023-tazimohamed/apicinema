import express, { Request, Response } from 'express';
import { AppDataSource } from '../../database/database';
import { AG } from '../../database/entities/AG';
import { AGUsecase } from '../../domain/ag-usecase';
import { createAGValidation, updateAGValidation, listAGValidation, agIdValidation } from '../validators/ag-validator';
import { generateValidationErrorMessage } from '../validators/generate-validation-message';

export const AGHandler = (app: express.Express) => {
    app.get("/ag", async (req: Request, res: Response) => {
        const validation = listAGValidation.validate(req.query);

        if (validation.error) {
            res.status(400).send(generateValidationErrorMessage(validation.error.details));
            return;
        }

        const listAGRequest = validation.value;
        let limit = 20;
        if (listAGRequest.limit) {
            limit = listAGRequest.limit;
        }
        const page = listAGRequest.page ?? 1;

        try {
            const agUsecase = new AGUsecase(AppDataSource);
            const listAGs = await agUsecase.listAGs({ ...listAGRequest, page, limit });
            res.status(200).send(listAGs);
        } catch (error) {
            console.log(error);
            res.status(500).send({ error: "Internal error" });
        }
    });

    app.post("/ag", async (req: Request, res: Response) => {
        const validation = createAGValidation.validate(req.body);

        if (validation.error) {
            res.status(400).send(generateValidationErrorMessage(validation.error.details));
            return;
        }
        const agRequest = validation.value;

        const agRepo = AppDataSource.getRepository(AG);

        try {
            const agCreated = await agRepo.save(agRequest);
            res.status(201).send(agCreated);
        } catch (error) {
            console.log(error);
            res.status(500).send({ error: "Internal error" });
        }
    });

    app.delete("/ag/:id", async (req: Request, res: Response) => {
        try {
            const validationResult = agIdValidation.validate(req.params);

            if (validationResult.error) {
                res.status(400).send(generateValidationErrorMessage(validationResult.error.details));
                return;
            }

            const agId = validationResult.value.id;

            const agRepo = AppDataSource.getRepository(AG);

            const ag = await agRepo.findOneBy({ id: agId });

            if (!ag) {
                res.status(404).send({ error: `AG ${agId} not found` });
                return;
            }

            await agRepo.delete(agId);

            res.status(204).send();
        } catch (error) {
            console.log(error);
            res.status(500).send({ error: "Internal error" });
        }
    });

    app.patch("/ag/:id", async (req: Request, res: Response) => {
        try {
            const validationResult = agIdValidation.validate(req.params);

            if (validationResult.error) {
                res.status(400).send(generateValidationErrorMessage(validationResult.error.details));
                return;
            }

            const agId = validationResult.value.id;

            const validation = updateAGValidation.validate(req.body);

            if (validation.error) {
                res.status(400).send(generateValidationErrorMessage(validation.error.details));
                return;
            }

            const agUpdate = validation.value;

            const agRepo = AppDataSource.getRepository(AG);

            const ag = await agRepo.findOneBy({ id: agId });

            if (!ag) {
                res.status(404).send({ error: `AG ${agId} not found` });
                return;
            }

            await agRepo.update(agId, agUpdate);

            res.status(204).send();
        } catch (error) {
            console.log(error);
            res.status(500).send({ error: "Internal error" });
        }
    });
}