import Joi from "joi";

export const createAGValidation = Joi.object<CreateAGValidationRequest>({
    nom: Joi.string().required(),
    date: Joi.date().required(),
    description: Joi.string().required(),
    type: Joi.string().required(),
    quorum: Joi.number().required()
});

export interface CreateAGValidationRequest {
    nom: string
    date: Date
    description: string
    type: String
    quorum: number
}

export const agIdValidation = Joi.object<AGIdRequest>({
    id: Joi.number().required(),
});

export interface AGIdRequest {
    id: number
}

export const updateAGValidation = Joi.object<UpdateAGRequest>({
    id: Joi.number().required(),
    nom: Joi.string().optional(),
    date: Joi.date().optional(),
    description: Joi.string().optional(),
    type: Joi.string().optional(),
    quorum: Joi.number().optional()
});

export interface UpdateAGRequest {
    id: number
    nom?: string
    date?: Date
    description?: string
    type?: String
    quorum?: number
}

export const listAGValidation = Joi.object<ListAGRequest>({
    page: Joi.number().min(1).optional(),
    limit: Joi.number().min(1).optional(),
    nom: Joi.string().optional(),
    date: Joi.date().optional(),
    description: Joi.string().optional(),
    type: Joi.string().optional(),
    quorum: Joi.number().optional()
});

export interface ListAGRequest {
    page: number
    limit: number
    nom?: string
    date?: Date
    description?: string
    type?: String
    quorum?: number
}