import { DataSource } from "typeorm";
import { AG } from "../database/entities/AG";

export interface ListAGRequest {
    page: number
    limit: number
    nom?: string
    date?: Date
    description?: string
    type?: String
    quorum?: number
}

export interface UpdateAGParams {
    nom?: string
    date?: Date
    description?: string
    type?: String
    quorum?: number
}

export class AGUsecase {
    constructor(private readonly db: DataSource) { }

    async listAGs(listAGRequest: ListAGRequest): Promise<{ AGs: AG[]; totalCount: number; }> {
        const query = this.db.createQueryBuilder(AG, 'ag');
        if (listAGRequest.nom) {
            query.andWhere("ag.nom = :nom", { nom: listAGRequest.nom });
        }

        if (listAGRequest.date) {
            query.andWhere("ag.date = :date", { date: listAGRequest.date });
        }

        if (listAGRequest.description) {
            query.andWhere("ag.description = :description", { description: listAGRequest.description });
        }

        if (listAGRequest.type) {
            query.andWhere("ag.type = :type", { type: listAGRequest.type });
        }

        query.skip((listAGRequest.page - 1) * listAGRequest.limit)
            .take(listAGRequest.limit);

        const [AGs, totalCount] = await query.getManyAndCount();
        return {
            AGs,
            totalCount
        };
    }

    async getOneAG(id: number): Promise<AG | null> {
        const query = this.db.createQueryBuilder(AG, 'ag')
            .where("ag.id = :id", { id: id });

        const ag = await query.getOne();

        if (!ag) {
            console.log({ error: `AG ${id} not found` });
            return null;
        }
        return ag;
    }


    async updateAG(id: number, {nom, date, description, type}: UpdateAGParams): Promise<AG | undefined> {
        const repo = this.db.getRepository(AG);
        const agFound = await repo.findOneBy({ id });
        if (agFound === null) return undefined;



        if (!agFound) {
            console.log({ error: `AG ${id} not found` });
            return undefined;
        }

        if (nom) {
            agFound.nom = nom;
        }
        if(date){
            agFound.date = date;
        }
        if(description){
            agFound.description = description;
        }
        if(type){
            agFound.type = type;
        }

        const agUpdated = await repo.save(agFound);
        return agUpdated;
    }
}