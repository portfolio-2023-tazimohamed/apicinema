import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';

@Entity()
export class AG {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nom: string;

    @Column()
    date: Date;

    @Column()
    description: string;

    @Column()
    type: String;

    @Column()
    quorum: number;



    constructor(id: number, nom: string,  date: Date, description: string, type: String, quorum: number) {
        this.id = id;
        this.nom = nom;
        this.description = description;
        this.type = type;
        this.date = date;
        this.quorum = quorum;    
    }
}